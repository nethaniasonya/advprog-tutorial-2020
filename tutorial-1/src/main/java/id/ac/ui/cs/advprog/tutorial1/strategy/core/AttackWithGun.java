package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
        //ToDo: Complete me
     public String attack(){
        return "attacking with gun";
    }

    public String getType() {
        return "gun";
    }
}
