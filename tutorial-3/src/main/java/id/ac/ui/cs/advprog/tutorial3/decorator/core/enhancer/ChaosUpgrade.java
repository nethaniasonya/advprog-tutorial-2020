package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class ChaosUpgrade extends Weapon {

    Weapon weapon;

    public ChaosUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 50-55 ++
    @Override
    public int getWeaponValue() {
        Random r = new Random();
        int rand = r.nextInt((55 - 50) + 1) + 50;
        return this.weapon.getWeaponValue() + rand;
    }

    @Override
    public String getDescription() {
        return "chaos upgrade";
    }
}
