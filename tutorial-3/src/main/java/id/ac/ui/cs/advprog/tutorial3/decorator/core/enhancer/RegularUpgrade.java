package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RegularUpgrade extends Weapon {

    Weapon weapon;

    public RegularUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 1-5 ++
    @Override
    public int getWeaponValue() {
        Random r = new Random();
        int rand = r.nextInt((5 - 1) + 1) + 1;
        return this.weapon.getWeaponValue() + rand;
    }

    @Override
    public String getDescription() {
        return "regular upgrade";
    }
}
