package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    String name;
    String role;
    List<Member> memberList;

    public PremiumMember(String name, String role){
        this.name = name;
        this.role = role;
        memberList = new ArrayList<>();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getRole() {
        return role;
    }

    @Override
    public void addChildMember(Member member) {
        if (memberList.size() < 3 || role.equals("Master"))
            memberList.add(member);
    }

    @Override
    public void removeChildMember(Member member) {
        memberList.remove(member);
    }

    @Override
    public List<Member> getChildMembers() {
        return memberList;
    }
}
