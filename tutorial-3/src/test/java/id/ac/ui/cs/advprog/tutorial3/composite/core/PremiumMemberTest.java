package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        assertEquals("Wati", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        assertEquals("Gold Merchant", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        int initSize = member.getChildMembers().size();
        Member child = new PremiumMember("Name", "Role");
        member.addChildMember(child);
        assertEquals(member.getChildMembers().size(), initSize + 1);
    }

    @Test
    public void testMethodRemoveChildMember() {
        Member child = new PremiumMember("Name", "Role");
        member.addChildMember(child);
        int initSize = member.getChildMembers().size();
        member.removeChildMember(child);
        assertEquals(member.getChildMembers().size(), initSize - 1);
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        member.addChildMember(new PremiumMember("One", "Role"));
        member.addChildMember(new PremiumMember("Two", "Role"));
        member.addChildMember(new PremiumMember("Three", "Role"));
        member.addChildMember(new PremiumMember("One", "Role"));
        assertEquals(member.getChildMembers().size(), 3);
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        Member master = new PremiumMember("Dummy", "Master");
        master.addChildMember(new PremiumMember("One", "Role"));
        master.addChildMember(new PremiumMember("Two", "Role"));
        master.addChildMember(new PremiumMember("Three", "Role"));
        master.addChildMember(new PremiumMember("One", "Role"));
        assertEquals(master.getChildMembers().size(), 4);
    }
}
