package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.ShiningArmor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.ShiningForce;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.ShiningBuster;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNull;


public class LordranAcademyTest {
    KnightAcademy lodranAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        lodranAcademy = new LordranAcademy();
        majesticKnight = lodranAcademy.getKnight("majestic");
        metalClusterKnight = lodranAcademy.getKnight("metal cluster");
        syntheticKnight = lodranAcademy.getKnight("synthetic");
    }

    @Test
    public void checkKnightInstances() {
        assertTrue(majesticKnight instanceof MajesticKnight);
        assertTrue(metalClusterKnight instanceof MetalClusterKnight);
        assertTrue(syntheticKnight instanceof SyntheticKnight);
    }

    @Test
    public void checkKnightNames() {
        assertEquals("Majestic Knight", majesticKnight.getName());
        assertEquals("Metal Cluster Knight", metalClusterKnight.getName());
        assertEquals("Synthetic Knight", syntheticKnight.getName());
    }

    @Test
    public void checkKnightDescriptions() {
        assertNull(majesticKnight.getSkill());
        assertTrue(majesticKnight.getArmor() instanceof ShiningArmor);
        assertTrue(majesticKnight.getWeapon() instanceof ShiningBuster);

        assertNull(metalClusterKnight.getWeapon());
        assertTrue(metalClusterKnight.getArmor() instanceof ShiningArmor);
        assertTrue(metalClusterKnight.getSkill() instanceof ShiningForce);

        assertNull(syntheticKnight.getArmor());
        assertTrue(syntheticKnight.getWeapon() instanceof ShiningBuster);
        assertTrue(syntheticKnight.getSkill() instanceof ShiningForce);
    }
}
