package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    @Mock
    private AcademyRepository academyRepository = new AcademyRepository();

    @InjectMocks
    private AcademyService academyService = new AcademyServiceImpl(academyRepository);

    @Test
    public void testProduceKnight(){
        academyService = new AcademyServiceImpl(new AcademyRepository());
        assertTrue(academyService.getKnightAcademies().size() > 0);
        academyService.produceKnight("Lordran", "majestic");
        assertTrue(academyService.getKnight() instanceof MajesticKnight);
    }

}
