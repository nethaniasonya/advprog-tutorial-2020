package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {
    HolyGrail holyGrail;

    @BeforeEach
    public void setUp(){
        holyGrail = new HolyGrail();
    }

    @Test
    public void testMakeAndGetAWish(){
        holyGrail.makeAWish("mau dapet duit");
        assertEquals("mau dapet duit", holyGrail.getHolyWish().toString());
    }
}
